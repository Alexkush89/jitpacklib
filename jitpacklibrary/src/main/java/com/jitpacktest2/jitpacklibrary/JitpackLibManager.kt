package com.jitpacktest2.jitpacklibrary


class JitpackLibManager {
    fun libFun(value: String) = value.length

    /**Method description
     * @return is this default value v2.0.1*/
    fun libFun2(newValue: String) = newValue == "default value"
}